import os
import json
import argparse

def scan_for_jest_tests(root_dir, file_patterns, exclude_dirs, max_recursion_depth=None):
    """Scans a Node.js project for Jest test files and creates a list.

    Args:
        root_dir (str): The root directory of your Node.js project.
        file_patterns (list): File extensions to consider as test files.
        exclude_dirs (list): Directories to skip during the scan.
        max_recursion_depth (int, optional): Maximum depth of subdirectory recursion.
    """

    test_files = []

    def should_scan_dir(path):
        return not any(excluded in path for excluded in exclude_dirs) and 'node_modules' not in path

    recursion_limit = 0  # Counter for recursion depth

    for root, dirs, files in os.walk(root_dir):
        if max_recursion_depth and recursion_limit >= max_recursion_depth:
            dirs[:] = []  # Stop recursion if max depth is reached
        else:
            for file in files:
                if file.endswith(tuple(file_patterns)):  
                    relative_path = os.path.relpath(os.path.join(root, file), root_dir)
                    test_files.append(relative_path)

            if should_scan_dir(root):
                recursion_limit += 1

    with open("test_list.txt", "w") as f:
        for test_file in test_files:
            f.write(test_file + "\n")

def load_config(config_file="test_scanner.json"):
    try:
        with open(config_file) as f:
            return json.load(f)
    except FileNotFoundError:
        return {}  

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Scan for Jest test files.")
    parser.add_argument("-r", "--root_dir", help="Custom root directory to scan")
    args = parser.parse_args()

    config = load_config()

    # Use CI_PROJECT_DIR if available, otherwise fallback to other logic 
    root_dir = args.root_dir or config.get("root_directory") or os.getenv("CI_PROJECT_DIR") or os.path.abspath(os.getcwd())  

    print(f"Determined Root Directory: {root_dir}")

    file_patterns = config.get("file_patterns", [".spec.js",".test.js", ".test.jsx", ".test.ts", ".test.tsx"])
    exclude_dirs = config.get("exclude_dirs", [])
    max_recursion_depth = config.get("max_recursion_depth")

    scan_for_jest_tests(root_dir, file_patterns, exclude_dirs, max_recursion_depth)