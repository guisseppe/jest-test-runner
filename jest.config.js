// For a detailed explanation regarding each configuration property, visit:
// https://jestjs.io/docs/en/configuration.html

module.exports = {
  coverageDirectory: "coverage",
  moduleNameMapper: {
    "@/(.*)$": "<rootDir>/src/$1"
  },
  rootDir: ".",
  testMatch: ["**/__tests__/**/*.js?(x)", "**/?(*.)+(spec|test).js?(x)", "**/test/**/*.js?(x)"], // Add the last pattern here
  testEnvironment: "node",
  transform: {
    "^.+\\.jsx?$": "babel-jest"
  }
};
