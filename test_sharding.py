import os
import sys
import json
import argparse

MAX_SHARDS = 4

class LogLevel:
    INFO = "INFO"
    WARNING = "WARNING"
    ERROR = "ERROR"

def log(level, message):
    """Prints a log message with appropriate colors.

    Args:
        level (LogLevel): The log level (INFO, WARNING, ERROR).
        message (str): The message to log.
    """
    color_codes = {
        LogLevel.INFO: "\033[92m",  # Green
        LogLevel.WARNING: "\033[93m",  # Yellow
        LogLevel.ERROR: "\033[91m",  # Red
    }
    reset_code = "\033[0m"
    print(f"{color_codes.get(level, '')}{level}: {message}{reset_code}")

def shard_test_list(file_path, num_shards):
    """Shards a test list file into multiple files, ensuring no duplicate tests across shards.

    Args:
        file_path (str): Path to the test list file.
        num_shards (int): Number of shards to create.
    """

    with open(file_path, "r") as f:
        test_files = set(f.readlines())  # Use a set to automatically remove duplicates

    tests_per_shard = len(test_files) // num_shards
    remainder = len(test_files) % num_shards

    shards = []  # Store shards as a list of lists
    for shard_num in range(num_shards):
        start_index = shard_num * tests_per_shard
        end_index = start_index + tests_per_shard + (1 if shard_num < remainder else 0)
        shards.append(list(test_files)[start_index:end_index])  # Convert set slice to list

    for shard_num, shard_files in enumerate(shards):
        with open(f"test_list_shard_{shard_num + 1}.txt", "w") as shard_file:
            shard_file.writelines(shard_files)


def modify_ci_test_command(new_value):
    with open('package.json', 'r') as file:
        data = json.load(file)
    
    if "scripts" in data and "ci-test" in data["scripts"]:
        test_script = data["scripts"]["ci-test"]
        new_script = test_script.replace(" -- ", new_value)
        return new_script
    else:
        log(LogLevel.ERROR, "Script 'ci-test' not found in package.json or 'scripts' field is missing")
        sys.exit(1) 


def run_shard(shard_num, test_runner):
    """Constructs the test command for a specific shard.

    Args:
        shard_num (int): The shard number to run.
        test_runner (str, optional): The test runner command (defaults to 'npm test').

    Returns:
        str: The constructed test command.
    """

    shard_file = f"test_list_shard_{shard_num}.txt"
    if os.path.exists(shard_file):
        with open(shard_file, 'r') as file:
            test_file_paths = file.readlines()
            test_file_paths = [path.strip() for path in test_file_paths]

        jest_command = [test_runner, *test_file_paths]
        return modify_ci_test_command(" ".join(jest_command))
    else:
        log(LogLevel.ERROR, f"Shard file not found: {shard_file}")
        return None


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Shard and run Jest tests.")
    parser.add_argument("test_list", help="Path to the test list file.")
    parser.add_argument("-s", "--shard", type=int, required=True, help="Shard number to run.")
    parser.add_argument("-n", "--num_shards", type=int, default=2, help="Number of shards to create.")
    parser.add_argument("-t", "--test_runner", default="npx jest", help="Test runner command.")
    args = parser.parse_args()

    if args.num_shards > MAX_SHARDS:
        log(LogLevel.WARNING, f"Maximum allowed shards is {MAX_SHARDS}. Exiting.")
        sys.exit(1)

    shard_test_list(args.test_list, args.num_shards)

    test_command = run_shard(args.shard, args.test_runner)
    if test_command:
        print(test_command) 
